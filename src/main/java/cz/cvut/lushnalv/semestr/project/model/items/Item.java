package cz.cvut.lushnalv.semestr.project.model.items;

import cz.cvut.lushnalv.semestr.project.model.Entity;
import cz.cvut.lushnalv.semestr.project.model.Position;

public abstract class Item implements Entity {
    private Position position;
    private int level;

    @Override
    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

}
