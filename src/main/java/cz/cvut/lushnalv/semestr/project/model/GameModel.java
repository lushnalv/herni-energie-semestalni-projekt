package cz.cvut.lushnalv.semestr.project.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cz.cvut.lushnalv.semestr.project.model.characters.Hero;
import cz.cvut.lushnalv.semestr.project.model.characters.Player;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class GameModel {
    private Map<Position, Entity> entityMap = new ConcurrentHashMap<>();

    public Set<Entity> getEntities() {
        return new HashSet<>(entityMap.values());
    }
//for json
    public void setEntities(Set<Entity> entities) {
        entityMap.clear();
        entities.forEach(it -> entityMap.put(it.getPosition(), it));
    }
//for json
    public void addEntity(Entity entity) {
        entityMap.put(entity.getPosition(), entity);
    }

    public void removeEntity(Entity entity) {
        entityMap.remove(entity.getPosition());
    }

    /**
     * Returns entity in position
     * @param position
     * @return
     */

    public Entity getByPosition(Position position) {
        return entityMap.get(position);
    }

    /**
     * Moves entity to new position
     * @param entity
     * @param position
     */
    public void updatePosition(Entity entity, Position position) {
        entityMap.remove(entity.getPosition());
        entity.setPosition(position);
        entityMap.put(position, entity);
    }
}
