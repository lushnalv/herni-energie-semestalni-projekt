# PŘÍRUČKA HRY KOLOBCÍ

<p>Cílem hry je projít úroveň, 
bojovat proti nepříteli pomocí zbrany a obrany.</p>

<p>Hráč může ovládat hrdinou pomocí kláves:
 nahoru, dolů, vlevo a vpravo.</p>

<p>Útok se provádí pohybem směrem k nepříteli.</p>

<p>Sbírejte štíty, které pomáhají odolat 
útokům nepřítele. 
Číslo na štítu označuje úroveň obrany.</p>

<p>Sbírejte zbraně, což pomáhá útočit na nepřítele.</p>

<p>Úroveň obrany a síla zbraně  je vidět přímo na panelu položek.</p>

<p>Hráč si může vybrat zbran a štit. 
Položku můžete vybrat kliknutím myši.</p>

<p>K dokončení úrovně a vychodu ven pres dveře musíte vzít klíč před tím.</p>

<p><strong>Přeji vám příjemnou hru!<strong></p>