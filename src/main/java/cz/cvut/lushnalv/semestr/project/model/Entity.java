package cz.cvut.lushnalv.semestr.project.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

public interface Entity {
    Position getPosition();

    void setPosition(Position position);

    default void setViewClass(String str) {}

    /**
     * Returns css class for UI
     * @return
     */
    default String getViewClass() {
        return getClass().getSimpleName().toLowerCase();
    }
}
