package cz.cvut.lushnalv.semestr.project.controller;

import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdValueInstantiator;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.type.SimpleType;
import cz.cvut.lushnalv.semestr.project.gui.GameView;
import cz.cvut.lushnalv.semestr.project.gui.PlayerView;
import cz.cvut.lushnalv.semestr.project.model.Entity;
import cz.cvut.lushnalv.semestr.project.model.GameModel;
import cz.cvut.lushnalv.semestr.project.model.Position;
import cz.cvut.lushnalv.semestr.project.model.characters.Enemy;
import cz.cvut.lushnalv.semestr.project.model.characters.Hero;
import cz.cvut.lushnalv.semestr.project.model.characters.Player;
import cz.cvut.lushnalv.semestr.project.model.gameitems.Door;
import cz.cvut.lushnalv.semestr.project.model.items.Item;
import cz.cvut.lushnalv.semestr.project.model.items.Key;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;


public class GameController {
    private static Logger log = LoggerFactory.getLogger(GameController.class);
    private GameView gameView;
    private PlayerView playerView;
    private GameModel model = new GameModel();
    private boolean stop = false;
    private ObjectMapper mapper;

    /**
     * Creates controller from gameView and playerView
     * @param gameView
     * @param playerView
     */
    public GameController(GameView gameView, PlayerView playerView) {
        this.gameView = gameView;
        this.playerView = playerView;
        this.gameView.setModel(model);
        this.playerView.setModel(model);
        this.mapper = new ObjectMapper();// saves objects to json
        this.mapper.enableDefaultTyping();
        SimpleModule module = new SimpleModule();
        module.addValueInstantiator(Enemy.class, new StdValueInstantiator(mapper.getDeserializationConfig(), SimpleType.construct(Enemy.class)) {
            @Override
            public boolean canCreateUsingDefault() {
                return true;
            }

            @Override
            public Object createUsingDefault(DeserializationContext ctxt) throws IOException {
                return new Enemy(GameController.this);
            }
        });
        module.addValueInstantiator(Player.class, new StdValueInstantiator(mapper.getDeserializationConfig(), SimpleType.construct(Player.class)) {
            @Override
            public boolean canCreateUsingDefault() {
                return true;
            }

            @Override
            public Object createUsingDefault(DeserializationContext ctxt) throws IOException {
                return Player.createInstance(GameController.this);
            }
        });
        mapper.registerModule(module);

    }
    /**
     * If player chooses continue game, program load previous game
     */
    private void loadLevel() {
        if (Files.exists(Paths.get("game.json"))) {
            loadGame();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Select");
            ButtonType buttonTypeOne = new ButtonType("Restart");
            ButtonType buttonTypeCancel = new ButtonType("Continue", ButtonBar.ButtonData.CANCEL_CLOSE);
            alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeCancel);
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == buttonTypeOne) {
                loadStartingLevel();
            }
        } else {
            loadStartingLevel();
        }
        gameView.refresh();
    }
    /**
     * If player chooses start nave game, program load new game
     */
    private void loadStartingLevel() {
        try (InputStream inputStream = getClass().getClassLoader().getResource("level/level.json").toURI().toURL().openStream()) {
            mapper.readerForUpdating(model).readValue(inputStream);
        } catch (IOException | URISyntaxException e) {
            log.error(e.getMessage(), e);
            Platform.exit();
        }
    }

    /**
     * Starts gameController
     */
    public void start() {
        loadLevel();
        gameView.refresh();
        playerView.refresh();
        startGameCycle();
    }
    /**
     * While !stop run gameCycle and save game any turn
     */
    private void startGameCycle() {
        new Thread(() -> {
            while (!stop) {
                Collection<Entity> entities = new HashSet<>(model.getEntities());
                log.info("Player's move");
                Player.getInstance().makeTurn();
                for (Entity entity : entities) {
                    if (entity instanceof Hero && !(entity instanceof Player)) {
                        log.info(entity.getClass().getSimpleName() + "'s move");
                        Hero hero = (Hero) entity;
                        hero.makeTurn();
                    }
                }
                saveGame();
            }
        }).start();
    }
    /**
     * Save game any turn
     */
    private void saveGame() {
        try {
            mapper.writeValue(new FileOutputStream("game.json"), model);
        } catch (IOException e) {
            log.error(e.getMessage(), e);
            Platform.exit();
        }
    }
    /**
     * If player chooses continue game, program load previous game
     */
    private void loadGame() {
        try {
            mapper.readerForUpdating(model).readValue(new FileInputStream("game.json"));
        } catch (IOException e) {
            log.error(e.getMessage(), e);
            Platform.exit();
        }
    }

    /**
     * Rules of move for heroes
     * @param hero
     * @param position
     */
    public void move(Hero hero, Position position) {
        //check that hero dont move abroad
        if (position.getX() < 0 || position.getX() > 15 || position.getY() < 0 || position.getY() > 11) {
            if (hero instanceof Enemy) {
                hero.setRemainingPoints(0);
            }return;
        }
        //check that cell doesnt have target
        Entity target = model.getByPosition(position);
        if (target == null) {
            model.updatePosition(hero, position);
            hero.setRemainingPoints(hero.getRemainingPoints() - 1);
            gameView.refresh();
        //check if cell have enemy
        } else if (target instanceof Hero) {
            hero.attack((Hero) target);
            //check that hero is enemy
        } else if (hero instanceof Enemy) {
            hero.setRemainingPoints(0);
            //if target is Item and her is Player
        } else if (target instanceof Item && hero instanceof Player) {
            Player player = (Player) hero;
            model.updatePosition(hero, position);
            player.getItems().add((Item) target);
            gameView.refresh();
            playerView.refresh();
            log.info(hero.getClass().getSimpleName() + " found " + target);
            // If target is exit and hero si player = exit from level
        } else if (target instanceof Door && hero instanceof Player) {
            if (Player.getInstance().getItems().stream().anyMatch(item -> item instanceof Key)) {
                Platform.runLater(()->{
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Game is finished");

                    ButtonType buttonTypeOne = new ButtonType("Restart");


                    alert.getButtonTypes().setAll(buttonTypeOne);

                    Optional<ButtonType> result = alert.showAndWait();
                    if (result.get() == buttonTypeOne) {
                        loadStartingLevel();
                        gameView.refresh();
                        playerView.refresh();
                    }

                });

            }
        }
    }


    /**
     * Removes Hero from the game
     * @param target
     */
    public void kill(Hero target) {
        model.removeEntity(target);
        gameView.refresh();
        if (target instanceof Player) {
            log.info("Game over");
            stop = true;
        }
    }
}
