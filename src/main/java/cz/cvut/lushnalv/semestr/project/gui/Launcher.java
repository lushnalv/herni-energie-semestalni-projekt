package cz.cvut.lushnalv.semestr.project.gui;

// We can't launch the application using Main class when maven-shade-plugin is used
// This launcher however fixes this somehow
public class Launcher {
    public static void main(String[] args) {
        Main.main(args);
    }
}
