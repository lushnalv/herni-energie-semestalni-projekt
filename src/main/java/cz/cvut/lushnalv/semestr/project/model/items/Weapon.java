package cz.cvut.lushnalv.semestr.project.model.items;

public class Weapon extends Item {
    private String viewClass;
    private int power;


    public String getViewClass() {
        return viewClass;
    }

    public void setViewClass(String viewClass) {
        this.viewClass = viewClass;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    @Override
    public String toString() {
        return  viewClass + " " + getPower() + " power";
    }
}
