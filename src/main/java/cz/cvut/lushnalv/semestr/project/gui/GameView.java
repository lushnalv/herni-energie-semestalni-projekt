package cz.cvut.lushnalv.semestr.project.gui;

import cz.cvut.lushnalv.semestr.project.model.GameModel;

public interface GameView {
    void setModel(GameModel model);

    /**
     * Refreshes UI according the model
     */
    void refresh();
}
