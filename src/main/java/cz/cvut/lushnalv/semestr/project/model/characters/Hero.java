package cz.cvut.lushnalv.semestr.project.model.characters;

import cz.cvut.lushnalv.semestr.project.controller.Command;
import cz.cvut.lushnalv.semestr.project.controller.GameController;
import cz.cvut.lushnalv.semestr.project.model.Entity;
import cz.cvut.lushnalv.semestr.project.model.Position;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class Hero implements Entity {
    private static Logger log = LoggerFactory.getLogger(Hero.class);
    protected GameController gameController;
    private Position position;

    private int hitPoints;
    private int power;
    private int level;
    private int actionPoints;
    private int remainingPoints;

    public Hero() {
    }

    public Hero(GameController gameController) {
        this.gameController = gameController;
    }


    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public int getRemainingPoints() {
        return remainingPoints;
    }

    public void setRemainingPoints(int remainingPoints) {
        this.remainingPoints = remainingPoints;
    }

    public int getActionPoints() {
        return actionPoints;
    }

    public void setActionPoints(int actionPoints) {
        this.actionPoints = actionPoints;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public int getHitPoints() {
        return hitPoints;
    }

    public void setHitPoints(int hitPoints) {
        this.hitPoints = hitPoints;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    /**
     * Executes hero's turn
     */
    public abstract void makeTurn();

    protected void execute(Command command) {
        switch (command) {
            case RIGHT:
                gameController.move(this, new Position(getPosition().getX() + 1, getPosition().getY()));
                break;
            case LEFT:
                gameController.move(this, new Position(getPosition().getX() - 1, getPosition().getY()));
                break;
            case UP:
                gameController.move(this, new Position(getPosition().getX(), getPosition().getY() - 1));
                break;
            case DOWN:
                gameController.move(this, new Position(getPosition().getX(), getPosition().getY() + 1));
                break;
        }
    }

    /**
     * Attack target
     * @param target
     */
    public void attack(Hero target) {
        int power = getPower();
        if (target instanceof Player) {
            Player player = (Player) target;
            if (player.getArmor() != null) {
                float powerF = (float) power;
                float res =  player.getArmor().getResistance();
                powerF = powerF - (1 - res/100F);
                power = (int) powerF;
            }
        }
        setRemainingPoints(0);
        target.setHitPoints(target.getHitPoints() - power);
        if (target.getHitPoints() <= 0) {
            gameController.kill(target);
        }
        log.info(getClass().getSimpleName() + "(" + getHitPoints() + ") Attacked " + target.getClass().getSimpleName() + " with power " + getPower() + " hitpoints remaining " + target.getHitPoints());
    }


}
