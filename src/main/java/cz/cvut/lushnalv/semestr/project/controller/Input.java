package cz.cvut.lushnalv.semestr.project.controller;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

public class Input {
    /**
     * Player command queue
     */
    public static BlockingQueue<Command> commandQueue = new LinkedBlockingDeque<>();
}
