package cz.cvut.lushnalv.semestr.project.model.gameitems;

import cz.cvut.lushnalv.semestr.project.model.Entity;
import cz.cvut.lushnalv.semestr.project.model.Position;

public class GameItem implements Entity {
    private Position position;

    @Override
    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }
}

