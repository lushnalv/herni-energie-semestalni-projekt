package cz.cvut.lushnalv.semestr.project.model.characters;

import cz.cvut.lushnalv.semestr.project.TestGameView;
import cz.cvut.lushnalv.semestr.project.TestPlayerView;
import cz.cvut.lushnalv.semestr.project.controller.GameController;
import cz.cvut.lushnalv.semestr.project.model.items.Armor;
import cz.cvut.lushnalv.semestr.project.model.items.Weapon;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HeroTest {

    private final GameController controller = new GameController(new TestGameView(),new TestPlayerView()) {
        @Override
        public void kill(Hero target) {

        }
    };

    @Test
    void defaultAttack() {
        Hero one = new Enemy(controller);
        one.setPower(5);
        Hero two = new Enemy(controller);
        two.setHitPoints(15);
        one.attack(two);

        assertEquals(10, two.getHitPoints());
    }

    @Test
    void attackWithArmor() {
        Hero one = new Enemy(controller);
        one.setPower(30);
        Player two = Player.createInstance(controller);
        Armor armor = new Armor();
        armor.setResistance(30);
        two.equip(armor);
        two.setHitPoints(100);
        one.attack(two);

        assertEquals(71, two.getHitPoints());
    }
    @Test
    void attackWithWeapon() {
        Hero one = new Enemy(controller);
        one.setHitPoints(10);
        Player two = Player.createInstance(controller);
        Weapon weapon = new Weapon();
        weapon.setPower(5);
        two.equip(weapon);
        two.attack(one);
        assertEquals(5, one.getHitPoints());
    }


}