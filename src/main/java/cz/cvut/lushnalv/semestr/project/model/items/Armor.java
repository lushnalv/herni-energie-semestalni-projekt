package cz.cvut.lushnalv.semestr.project.model.items;

public class Armor extends Item {
    private String viewClass;
    private int resistance;

    public String getViewClass() {
        return viewClass;
    }

    public void setViewClass(String viewClass) {
        this.viewClass = viewClass;
    }

    public int getResistance() {
        return resistance;
    }

    public void setResistance(int resistance) {
        this.resistance = resistance;
    }

    @Override
    public String toString() {
        return viewClass + " "  + getResistance() + " resistance";
    }

}
