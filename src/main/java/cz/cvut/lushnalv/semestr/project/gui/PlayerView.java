package cz.cvut.lushnalv.semestr.project.gui;

import cz.cvut.lushnalv.semestr.project.model.GameModel;

public interface PlayerView {
    void setModel(GameModel model);

    /**
     * Refreshes UI according the model
     */
    void refresh();
}
