package cz.cvut.lushnalv.semestr.project.model.characters;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cz.cvut.lushnalv.semestr.project.controller.Command;
import cz.cvut.lushnalv.semestr.project.controller.GameController;
import cz.cvut.lushnalv.semestr.project.controller.Input;
import cz.cvut.lushnalv.semestr.project.model.Position;
import cz.cvut.lushnalv.semestr.project.model.items.Armor;
import cz.cvut.lushnalv.semestr.project.model.items.Item;
import cz.cvut.lushnalv.semestr.project.model.items.Weapon;

import javax.xml.stream.events.Comment;
import java.nio.file.WatchEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

public class Player extends Hero {
    private static Player instance;
    private Armor armor;
    private Weapon weapon;

    public static Player getInstance() {
        return instance;
    }

    public static Player createInstance(GameController gameController) {
        if (instance == null) {
            instance = new Player(gameController);
        }
        return instance;
    }

    private List<Item> items = new ArrayList<>();

    private Player(GameController gameController) {
        super(gameController);
    }


    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    @Override
    public void makeTurn() {
        Input.commandQueue.clear();
        setRemainingPoints(getActionPoints());
        while (getRemainingPoints() > 0) {
            try {
                execute(Input.commandQueue.take());
            } catch (InterruptedException e) {
            }
        }


    }
//for json
    public void setArmor(Armor armor) {
        this.armor = armor;
    }
// for json
    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    public Armor getArmor() {
        return armor;
    }
//for json
    public Weapon getWeapon() {
        return weapon;
    }
    /**
     * Quantity of power
     */
    @Override
    public int getPower() {
        int weaponPower = 0;
        if (weapon != null) {
            weaponPower = weapon.getPower();
        }
        return super.getPower() + weaponPower;
    }

    @Override
    public String getViewClass() {
        return "kolpbok";
    }

    /**
     * Equip item
     */
    public void equip(Item item) {
        if (item instanceof Armor) {
            if (armor != null) {
                unequip(armor);
            }
            this.armor = (Armor) item;
            items.remove(item);
        } else if (item instanceof Weapon) {
            if (weapon != null) {
                unequip(weapon);
            }
            this.weapon = (Weapon) item;
            items.remove(item);
        }
    }

    /**
     * List of players equipment
     */
    @JsonIgnore
    public List<Item> getEquipment() {
        List<Item> result = new ArrayList<>();
        if (weapon != null) {
            result.add(weapon);
        }
        if (armor != null) {
            result.add(armor);
        }
        return result;
    }
    /**
     * Unequip item
     */
    public void unequip(Item item) {
        if (item instanceof Weapon) {
            weapon = null;
            items.add(item);
        }
        if (item instanceof Armor) {
            armor = null;
            items.add(item);
        }
    }
}
