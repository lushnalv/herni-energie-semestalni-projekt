package cz.cvut.lushnalv.semestr.project.gui;

import cz.cvut.lushnalv.semestr.project.model.Entity;
import cz.cvut.lushnalv.semestr.project.model.GameModel;
import cz.cvut.lushnalv.semestr.project.model.Position;
import javafx.application.Platform;
import javafx.scene.layout.Pane;


public class GameViewImpl extends Pane implements GameView {

    private GameModel model;

    public GameViewImpl() {
        getStyleClass().add("game-field");
        setMinWidth(1024);
        setMinHeight(768);
        setMaxWidth(1024);
        setMaxHeight(768);

    }
    /**
     * Sets model
     */
    @Override
    public void setModel(GameModel model) {
        this.model = model;
    }

    /**
     * Refresh game field for player
     */
    @Override
    public void refresh() {
        Platform.runLater(() -> {
            getChildren().clear();
            for (Entity entity : model.getEntities()) {
                Pane pane = new Pane();

                pane.getStyleClass().add(entity.getViewClass());
                Position viewPosition = entity.getPosition().getViewPosition();
                pane.relocate(viewPosition.getX(), viewPosition.getY());

                getChildren().add(pane);
            }
        });
    }
}
