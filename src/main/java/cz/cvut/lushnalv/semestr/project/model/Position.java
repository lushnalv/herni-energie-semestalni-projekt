package cz.cvut.lushnalv.semestr.project.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Objects;



public class Position {
    private static final int SIZE_OF_CELL = 64;
    private int x;
    private int y;

    public Position() {
    }

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }
//for json
    public void setX(int x) {
        this.x = x;
    }
//for json
    public void setY(int y) {
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    /**
     * Returns position for UI
     * @return
     */
    @JsonIgnore
    public Position getViewPosition(){
        return new Position(getX()*SIZE_OF_CELL, getY()*SIZE_OF_CELL);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Position)) return false;
        Position position = (Position) o;
        return getX() == position.getX() &&
                getY() == position.getY();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getX(), getY());
    }
}

