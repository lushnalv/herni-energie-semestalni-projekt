package cz.cvut.lushnalv.semestr.project.model.items;

public class Key extends Item {
    private String viewClass;



    public String getViewClass() {
        return viewClass;
    }

    public void setViewClass(String viewClass) {
        this.viewClass = viewClass;
    }



    @Override
    public String toString() {
        return  viewClass;
    }
}
