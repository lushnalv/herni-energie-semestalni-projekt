package cz.cvut.lushnalv.semestr.project.gui;

import cz.cvut.lushnalv.semestr.project.controller.Command;
import cz.cvut.lushnalv.semestr.project.controller.GameController;
import cz.cvut.lushnalv.semestr.project.controller.Input;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class Main extends Application {
    @Override
    public void start(Stage stage) throws Exception {
        //Exit application on window close
        stage.setOnCloseRequest(t -> {
            Platform.exit();
            System.exit(0);
        });
        //Creating UI controls
        GameView gameView = new GameViewImpl();
        PlayerView playerView = new PlayerViewImpl();
        HBox hBox = new HBox();
        hBox.getChildren().add((Node)gameView);
        hBox.getChildren().add((Node)playerView);
        Scene scene = new Scene(hBox);
        //Listen keyboard for user commands
        scene.addEventHandler(KeyEvent.KEY_RELEASED, (key)->{
            switch(key.getCode()){
                case UP: Input.commandQueue.add(Command.UP); break;
                case DOWN: Input.commandQueue.add(Command.DOWN); break;
                case LEFT: Input.commandQueue.add(Command.LEFT); break;
                case RIGHT: Input.commandQueue.add(Command.RIGHT); break;
                }

        });

        stage.setScene(scene);

        scene.getStylesheets().add("main.css");


        GameController controller = new GameController(gameView, playerView);
        controller.start();
        stage.show();
    }

    public static void main(String...args){
        launch(args);
    }
}
