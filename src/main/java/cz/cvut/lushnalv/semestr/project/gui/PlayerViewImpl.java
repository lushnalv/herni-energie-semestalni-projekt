package cz.cvut.lushnalv.semestr.project.gui;

import cz.cvut.lushnalv.semestr.project.model.GameModel;
import cz.cvut.lushnalv.semestr.project.model.characters.Player;
import cz.cvut.lushnalv.semestr.project.model.items.Item;
import javafx.application.Platform;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

public class PlayerViewImpl extends Pane implements PlayerView {

    public static final int WIDTH = 200;
    private GameModel model;
    private VBox equipmentPanel;
    private VBox inventoryPanel;
    // create field for inventory and equipment
    public PlayerViewImpl() {
        setMinWidth(WIDTH);
        setMinHeight(768);
        setMaxWidth(WIDTH);
        setMaxHeight(768);

        VBox vBox = new VBox();
        getChildren().add(vBox);

        vBox.getChildren().add(new Label("Equipment"));
        equipmentPanel = new VBox();
        vBox.getChildren().add(equipmentPanel);
        equipmentPanel.setMinWidth(WIDTH);
        equipmentPanel.setMinHeight(200);

        vBox.getChildren().add(new Label("Inventory"));
        inventoryPanel = new VBox();
        vBox.getChildren().add(inventoryPanel);
        inventoryPanel.setMinWidth(WIDTH);
        inventoryPanel.setMinHeight(568);

    }
    /**
     * Sets model
     */
    @Override
    public void setModel(GameModel model) {
        this.model = model;
    }
    /**
     * Refreshes field of inventory and equipment
     */
    @Override
    public void refresh() {
        Platform.runLater(()->{
            inventoryPanel.getChildren().clear();
            for (Item item : Player.getInstance().getItems()) {
                Button itemButton = new Button(item.toString());
                itemButton.setOnAction(it->{
                    Player.getInstance().equip(item);
                    refresh();
                });
                inventoryPanel.getChildren().add(itemButton);
            }

            equipmentPanel.getChildren().clear();
            for (Item item : Player.getInstance().getEquipment()) {
                Button itemButton = new Button(item.toString());
                itemButton.setOnAction(it->{
                    Player.getInstance().unequip(item);
                    refresh();
                });
                equipmentPanel.getChildren().add(itemButton);
            }
        });
    }
}
