package cz.cvut.lushnalv.semestr.project.model.characters;

import cz.cvut.lushnalv.semestr.project.controller.Command;
import cz.cvut.lushnalv.semestr.project.controller.GameController;

public class Enemy extends Hero {

    private Command command;

    public Enemy(GameController gameController) {
        super(gameController);
    }
    //for json
    public Command getCommand() {
        return command;
    }
    //for json
    public void setCommand(Command command) {
        this.command = command;
    }


    /**
     * Make  turn for hero
     */
    @Override
    public void makeTurn() {
        setRemainingPoints(getActionPoints());
        while (getRemainingPoints() > 0) {
            execute(command);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
            }
        }
        switch (command) {
            case UP:
                command = Command.DOWN;
                break;
            case DOWN:
                command = Command.UP;
                break;
            case RIGHT:
                command = Command.LEFT;
                break;
            case LEFT:
                command = Command.RIGHT;
                break;
        }
    }
}
