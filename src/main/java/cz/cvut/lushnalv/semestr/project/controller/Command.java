package cz.cvut.lushnalv.semestr.project.controller;

/**
 * Command for hero
 */
public enum Command {
    UP, DOWN, LEFT, RIGHT
}
